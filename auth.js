import { oauth } from 'react-native-force';

class Auth {

	constructor() {
		this.loginData = {};
	}

	getUserId() {
		return this.loginData.userId;
	}

	authenticate() {
		
		return new Promise( ( resolve, reject ) => {
			
			const onSuccess = loginData => {
				this.loginData = loginData;
				resolve( loginData );
			};

			oauth.getAuthCredentials(
				onSuccess.bind( this ), // already logged in
				() => {
					oauth.authenticate(
						onSuccess.bind( this ),
						error => reject( error )
					);
				} );
		} );
	}
}

export default new Auth();