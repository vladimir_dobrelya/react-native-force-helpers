import { net } from 'react-native-force';

class Database {

	buildSoql( { fields, object, where, orderBy } ) {

		let result = `SELECT ${ fields.join( ',' ) } FROM ${ object }`;

		if ( where ) {
			result += ` WHERE ${ where }`;
		}

		if ( orderBy ) {
			result += ` ORDER BY ${ orderBy.join( ',' ) }`;
		}

		return result;
	}

	query( soql ) {

		return new Promise( ( resolve, reject ) => {
			net.query( this._optimizeQuery( soql ), response => resolve( response ), error => reject( error ) );
		} );
	}

	_optimizeQuery( soql ) {
		return soql.replace( /[\n\t]\s\s+/g, '' );
	}
}

export default new Database();